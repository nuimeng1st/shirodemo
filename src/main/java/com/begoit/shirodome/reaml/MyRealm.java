/*
 * Copyright (C), 2016-2017, 江苏北弓智能科技有限公司
 * FileName: MyRealm1.java
 * Author:   wangxiao
 * Date:     2017年8月26日 下午2:42:22
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 * CHENXJ						测试提交
 */
package com.begoit.shirodome.reaml;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.realm.Realm;

/**
 * 〈功能描述: <br> 
 *	
 * @author wangxiao
 */
public class MyRealm implements Realm {

	/* (non-Javadoc)
	 * @see org.apache.shiro.realm.Realm#getName()
	 */
	public String getName() {
		return "myRealm";
	}

	/* (non-Javadoc)
	 * @see org.apache.shiro.realm.Realm#supports(org.apache.shiro.authc.AuthenticationToken)
	 */
	public boolean supports(AuthenticationToken token) {
		return token instanceof UsernamePasswordToken;
	}

	/* (non-Javadoc)
	 * @see org.apache.shiro.realm.Realm#getAuthenticationInfo(org.apache.shiro.authc.AuthenticationToken)
	 */
	public AuthenticationInfo getAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		System.err.println("myRealm:"+getName());
		String username = token.getPrincipal().toString();
		String password = new String((char[])token.getCredentials());
		if (!"tom".equals(username) & !"jerry".equals(username)) {
			throw new UnknownAccountException();
		}
		if (!"123".equals(password)) {
			throw new IncorrectCredentialsException();
		}
		return new SimpleAuthenticationInfo(username, password, getName());
	}

}
