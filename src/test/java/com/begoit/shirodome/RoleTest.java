/*
 * Copyright (C), 2016-2017, 江苏北弓智能科技有限公司
 * FileName: RoleTest.java
 * Author:   wangxiao
 * Date:     2017年8月28日 下午4:29:15
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package com.begoit.shirodome;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.UnauthorizedException;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;
import org.junit.Test;

/**
 * 〈功能描述: <br> 
 *	subject.checkRole(role)没有权限时抛出AuthenticationException异常
 *	subject.hasRole(role)返回是否拥有权限的boolean结果
 *	Shiro提供的checkRole/checkRoles和hasRole/hasAllRoles
 * @author wangxiao
 */
public class RoleTest {
	@Test
	public void test(){
		Factory<SecurityManager> factory = new IniSecurityManagerFactory("classpath:shiro-role.ini");
		SecurityManager sm = factory.getInstance();
		SecurityUtils.setSecurityManager(sm);
		Subject subject = SecurityUtils.getSubject();
		UsernamePasswordToken token = new UsernamePasswordToken("jerry", "123");
		String role = "role1";
		try {
			System.out.println("***************************");
			System.out.println(token);
			System.out.println("***************************");
			subject.login(token);
			System.err.println("用户:["+token.getUsername()+"]登录成功");
			System.err.println("用户:["+token.getUsername()+"]拥有角色:["+role+"]");
			System.err.println("用户:["+token.getUsername()+"]是否拥有角色:["+role+"]:"+subject.hasRole(role));
			subject.checkRole(role);
		} catch (AuthenticationException e) {
			e.printStackTrace();
			System.err.println(e.getMessage());
			System.err.println("登录失败");
		}catch (UnauthorizedException e) {
			System.err.println("用户:["+token.getUsername()+"]没有角色:["+role+"]");
		}
	}
}