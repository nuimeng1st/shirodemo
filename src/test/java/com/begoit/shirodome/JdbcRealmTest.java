/*
 * Copyright (C), 2016-2017, 江苏北弓智能科技有限公司
 * FileName: JdbcRealmTest.java
 * Author:   wangxiao
 * Date:     2017年8月26日 下午4:55:29
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package com.begoit.shirodome;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;
import org.junit.Test;

/**
 * 〈功能描述: <br> 
 *	
 * @author wangxiao
 */
public class JdbcRealmTest {
	@Test
	public void test(){
		Factory<SecurityManager> factory = new IniSecurityManagerFactory("classpath:shiro-jdbc-realm.ini");
		SecurityManager sm = factory.getInstance();
		SecurityUtils.setSecurityManager(sm);
		Subject subject = SecurityUtils.getSubject();
		UsernamePasswordToken token = new UsernamePasswordToken("jerry", "456");
		try {
			System.out.println("***************************");
			System.out.println(token);
			System.out.println("***************************");
			subject.login(token);
			System.err.println("登录状态:"+subject.isAuthenticated());
			System.out.println("登录成功");
		} catch (AuthenticationException e) {
			System.err.println(e.getMessage());
			System.err.println("登录失败");
		}
	}
}
